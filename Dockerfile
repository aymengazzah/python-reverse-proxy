# Utiliser l'image de base Debian Bookworm Slim
FROM debian:bookworm-slim

# Mettre à jour et installer les dépendances nécessaires
RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    python3-ldap3 \
    python3-requests \
    python3-urllib3 \
    && apt-get clean

# Copier le script dans le conteneur
COPY server.py /usr/local/bin/server.py

# Définir les variables d'environnement par défaut (peuvent être surchargées)
ENV LDAP_SERVER=localhost \
    LDAP_PORT=636 \
    LDAP_USE_SSL=True \
    LDAP_START_TLS=False \
    LDAP_SEARCH_BASE_DN="ou=annuaire,dc=domain,dc=fr" \
    LDAP_SSL_SKIP_VERIFY=False \
    PROXY_TARGET=https://en.wikipedia.org \
    HTTPS_PROXY_MODE=False

COPY ./*.pem /etc/certs/
RUN chown root:root /etc/certs/*

# Rendre le script exécutable
RUN chmod +x /usr/local/bin/server.py

# Exposer le port 9999
EXPOSE 9999

# Définir le point d'entrée pour exécuter le script
ENTRYPOINT ["python3", "/usr/local/bin/server.py"]
