# Reverse Proxy avec Authentification LDAP

Ce script Python implémente un serveur proxy HTTP/HTTPS avec une authentification LDAP de base. Le proxy peut être configuré pour rediriger les requêtes vers une cible spécifique, tout en exigeant une authentification LDAP pour accéder au service proxy.

## Prérequis

- Python 3.x
- Bibliothèques Python :
  - `http.server`
  - `os`
  - `sys`
  - `requests`
  - `base64`
  - `ssl`
  - `socketserver`
  - `ldap3`
  - `urllib3`

## Configuration

Les paramètres du proxy peuvent être configurés via des variables d'environnement ou directement dans le script. Voici les paramètres disponibles avec leurs valeurs par défaut :

- `LDAP_SERVER`: Adresse du serveur LDAP (par défaut: `localhost`).
- `LDAP_PORT`: Port du serveur LDAP (par défaut: `636`).
- `LDAP_USE_SSL`: Utiliser SSL pour la connexion LDAP (par défaut: `True`).
- `LDAP_START_TLS`: Démarrer TLS pour la connexion LDAP (par défaut: `False`).
- `LDAP_SSL_SKIP_VERIFY`: Ignorer la vérification du certificat SSL pour LDAP (par défaut: `False`).
- `LDAP_SEARCH_BASE_DN`: Base DN pour la recherche LDAP (par défaut: `ou=annuaire,dc=domain,dc=fr`).
- `PROXY_TARGET`: Cible vers laquelle rediriger les requêtes (par défaut: `https://en.wikipedia.org`).
- `PORT`: Port sur lequel le serveur proxy écoute les connexions entrantes (par défaut: `9999`).
- `HTTPS_PROXY`: Activer le mode HTTPS pour le serveur proxy (par défaut: `False`).
- `CERT_FILE`: Chemin vers le fichier de certificat SSL (par défaut: `cert.pem`).
- `KEY_FILE`: Chemin vers le fichier de clé SSL (par défaut: `key.pem`).

## Utilisation

Pour exécuter le serveur proxy, exécutez simplement le script Python. Vous pouvez personnaliser le comportement en définissant les variables d'environnement appropriées ou en modifiant les valeurs directement dans le script.

### Génération de certificats autosignés 

```bash
openssl req -x509 -newkey rsa:4096 -sha256 -days 36500 -nodes -keyout key.pem -out cert.pem -subj "/C=FR/ST=Ile-de-France/L=Paris/O=MonOrganisation/OU=MonUnite/CN=www.mon-site.com/emailAddress=admin@mon-site.com"
```
### Lancer le script

```bash
python3 server.py


