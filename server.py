#!/usr/bin/env python3


import logging
import sys
from http.server import BaseHTTPRequestHandler, HTTPServer
import argparse, os, requests, base64, ssl
from socketserver import ThreadingMixIn
from ldap3 import Server, Connection, ALL, SUBTREE, Tls
import urllib3
import ssl
import ldap

# Configure logging
logging.basicConfig(level=logging.INFO, stream=sys.stdout, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger()

# Désactiver les avertissements InsecureRequestWarning
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Configuration LDAP
LDAP_SERVER = os.environ.get('LDAP_SERVER', 'localhost')
LDAP_PORT = int(os.environ.get('LDAP_PORT', 636))
LDAP_USE_SSL = os.environ.get('LDAP_USE_SSL', 'True').lower() == 'true'
LDAP_START_TLS = os.environ.get('LDAP_START_TLS', 'False').lower() == 'true'
LDAP_SEARCH_BASE_DN = os.environ.get('LDAP_SEARCH_BASE_DN', "ou=annuaire,dc=domain,dc=fr")
LDAP_SSL_SKIP_VERIFY = os.environ.get('LDAP_SSL_SKIP_VERIFY', 'False').lower() == 'true'

# Configuration du proxy
PROXY_TARGET = os.environ.get('PROXY_TARGET', 'https://fr.wikipedia.org')
PROXY_SCHEME = PROXY_TARGET.split('://')[0]
PROXY_HOST = PROXY_TARGET.split('://')[1]
HTTPS_PROXY_MODE = os.environ.get('HTTPS_PROXY_MODE', 'False').lower() == 'true'

def merge_two_dicts(x, y):
    return x | y

def set_header():
    headers = {
        'Host': PROXY_HOST
    }
    return headers


def ldap_authenticate(username, password):
    ldap_server = f"ldap://{LDAP_SERVER}:{LDAP_PORT}"
    search_base_dn = LDAP_SEARCH_BASE_DN
    search_filter = f"(uid={username})"
    ldap.set_option(ldap.OPT_REFERRALS, 0)
    conn = ldap.initialize(ldap_server)
    try:
        conn.simple_bind_s()
        result = conn.search_s(search_base_dn, ldap.SCOPE_SUBTREE, search_filter, ['dn'])
        if not result:
            logger.error(f"User {username} not found in LDAP directory.")
            return False
        user_dn = result[0][0]
        conn.simple_bind_s(user_dn, password)
        return True
    except ldap.INVALID_CREDENTIALS:
        logger.error("Invalid credentials")
        return False
    except ldap.LDAPError as e:
        logger.error(f"LDAP error: {e}")
        return False
    finally:
        conn.unbind_s()

class ProxyHTTPRequestHandler(BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.0'

    def authenticate(self):
        auth_header = self.headers.get('Authorization')
        if auth_header is None:
            self.send_auth_challenge()
            return False

        auth_type, credentials = auth_header.split(' ', 1)
        if auth_type.lower() != 'basic':
            self.send_auth_challenge()
            return False

        decoded_credentials = base64.b64decode(credentials).decode('utf-8')
        username, password = decoded_credentials.split(':', 1)
        if not ldap_authenticate(username, password):
            self.send_auth_challenge()
            return False

        return True

    def send_auth_challenge(self):
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm="Login Required"')
        self.end_headers()

    def do_HEAD(self):
        if not self.authenticate():
            return
        self.do_GET(body=False)

    def do_GET(self, body=True):
        self.handle_request('GET', body)

    def do_POST(self, body=True):
        self.handle_request('POST', body)

    def do_PATCH(self, body=True):
        self.handle_request('PATCH', body)

    def handle_request(self, method, body=True):
        if not self.authenticate():
            return
        sent = False
        try:
            url = f'{PROXY_TARGET}{self.path}'
            content_len = int(self.headers.get('Content-Length', 0))
            req_body = self.rfile.read(content_len) if method in ['POST', 'PATCH'] else None
            req_header = self.parse_headers()

            logger.info(f"Request Headers: {req_header}")
            logger.info(f"Request URL: {url}")
            logger.info(f"Request Body: {req_body}")

            session = requests.Session()
            request_method = getattr(session, method.lower())
            resp = request_method(url, headers=merge_two_dicts(req_header, set_header()), data=req_body, verify=False, allow_redirects=False, timeout=900)

            while resp.status_code in [301, 302, 303, 307, 308]:
                url = resp.headers['Location']
                if url.startswith('/'):
                    url = f'{PROXY_SCHEME}://{PROXY_HOST}{url}'
                resp = request_method(url, headers=merge_two_dicts(req_header, set_header()), data=req_body, verify=False, allow_redirects=False, timeout=900)

            sent = True
            self.send_resp_headers(resp)
            if body:
                self.wfile.write(resp.content)
            return
        except requests.exceptions.RequestException as e:
            logger.error(f'Error in {method} request: {e}')
        finally:
            if not sent:
                self.send_error(404, 'error trying to proxy')

    def parse_headers(self):
        req_header = {}
        for key, value in self.headers.items():
            if key.lower() == 'host':
                req_header[key] = PROXY_HOST
            else:
                req_header[key] = value
        return req_header

    def send_resp_headers(self, resp):
        self.send_response(resp.status_code)
        for key, value in resp.headers.items():
            if key.lower() not in ['content-encoding', 'transfer-encoding', 'content-length']:
                self.send_header(key, value)
        self.send_header('Content-Length', len(resp.content))
        self.end_headers()

def parse_args(argv=sys.argv[1:]):
    parser = argparse.ArgumentParser(description='Proxy HTTP requests')
    parser.add_argument('--port', dest='port', type=int, default=9999,
                        help='serve HTTP requests on specified port (default: random)')
    parser.add_argument('--proxy-target', dest='proxy_target', type=str, default=None,
                        help='hostname to be processed')
    parser.add_argument('--https', dest='https', action='store_true',
                        help='run proxy in HTTPS mode')
    args = parser.parse_args(argv)
    return args

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

def main(argv=sys.argv[1:]):
    global PROXY_TARGET, PROXY_SCHEME, PROXY_HOST, HTTPS_PROXY_MODE
    args = parse_args(argv)
    if args.proxy_target:
        PROXY_TARGET = args.proxy_target
        PROXY_SCHEME = PROXY_TARGET.split('://')[0]
        PROXY_HOST = args.proxy_target.split('://')[1]
    if args.https:
        HTTPS_PROXY_MODE = True

    REVERSE_PROXY_PORT = args.port  # Utilisation du port par défaut spécifié
    logger.info(f'ENVIRONNEMENT:')
    logger.info(f'Param LDAP_SERVER = {LDAP_SERVER}')
    logger.info(f'Param LDAP_PORT = {LDAP_PORT}')
    logger.info(f'Param LDAP_USE_SSL = {LDAP_USE_SSL}')
    logger.info(f'Param LDAP_START_TLS = {LDAP_START_TLS}')
    logger.info(f'Param LDAP_SSL_SKIP_VERIFY = {LDAP_SSL_SKIP_VERIFY}')
    logger.info(f'Param LDAP_SEARCH_BASE_DN = {LDAP_SEARCH_BASE_DN}')
    logger.info(f'Param PROXY_TARGET = {PROXY_TARGET}')
    logger.info(f'Param PROXY_SCHEME = {PROXY_SCHEME}')
    logger.info(f'Param PROXY_HOST = {PROXY_HOST}')
    logger.info(f'Param HTTPS_PROXY_MODE = {HTTPS_PROXY_MODE}')
    server_address = ('0.0.0.0', REVERSE_PROXY_PORT)
    httpd = ThreadedHTTPServer(server_address, ProxyHTTPRequestHandler)
    if HTTPS_PROXY_MODE:
        context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        context.load_cert_chain(certfile='/etc/certs/cert.pem', keyfile='/etc/certs/key.pem')
        httpd.socket = context.wrap_socket(httpd.socket, server_side=True)
        logger.info(f'Serveur reverse-proxy pour {PROXY_TARGET} accessible depuis https://localhost:{REVERSE_PROXY_PORT}...')
    else:
        logger.info(f'Serveur reverse-proxy pour {PROXY_TARGET} accessible depuis http://localhost:{REVERSE_PROXY_PORT}...')
    httpd.serve_forever()

if __name__ == '__main__':
    main()